import mysql2 from 'mysql2';
export default callback => {
	const connection = mysql2.createConnection({
		host: 'localhost',
		user: 'root',
		database: 'test',
		password: '1234',
	});
	// connect to a database if needed, then pass it to `callback`:
	callback(connection);
}
